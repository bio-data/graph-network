#!/usr/bin/python3
# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# The true delight is in the finding out rather than in the knowing.
# - Isaac Asimov

# %% [markdown]
# # Basic graph analysis demo on human brain data

# %%
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import pandas as pd

# %% [markdown]
# ## Functional connectivity
#
# (fc.csv) data are derived from
#
# <https://en.wikipedia.org/wiki/Resting_state_fMRI>
#
# ![image0.png](./images/image0.png)

# %% [markdown]
# ## Diffusion Tensor Imaging
#
# (dti.csv) data are derived from:
# <https://en.wikipedia.org/wiki/Diffusion_MRI>
#
# ![image1.png](./images/image1.png)

# %% [markdown]
# fc.csv and dti.csv are fully connected weighted matrices,
# derived from the two methods above.

# %%
matrix = np.genfromtxt("fc.csv", delimiter=",")
matrix_dti = np.genfromtxt("dti.csv", delimiter=",")

# %% [markdown]
# # Weight distributions
#
# ## Whas is the range and mean of weights in the fully connected network?
#
# ## What is the size of the network?
#
# ## How many connections does it have?

# %%
print(type(matrix))
print(matrix.shape)
print("Number of cells in the matrix:", 188 * 188)
print(matrix.max())
print(matrix.min())
print(matrix.mean())
matrix

# %%
print(matrix_dti.shape)
print(matrix_dti.max())
print(matrix_dti.min())
print(matrix_dti.mean())

# %%
# The weight distributions
plt.rcParams["figure.figsize"] = (7, 7)
plt.hist(matrix.ravel())

# %%
# The other weight distributions
plt.rcParams["figure.figsize"] = (7, 7)
plt.hist(matrix_dti.ravel())

# %%
# plt.rcParams["figure.figsize"] = (10, 10)
plt.boxplot(matrix.ravel())

# %% [markdown]
# Some statistics are valid on fully connected weighted networks,
# but not that many.
# Often we will threshold (as below) when required for some statistics,
# but preserve the full weight scheme where possible.

# %%
# threshold / binarize into a graph
matrix[abs(matrix) < 0.2] = 0
matrix = abs(matrix)
# percent connected
1 - (matrix == 0).sum() / 188**2

# %%
# threshold / binarize dti into a graph
matrix_dti[matrix_dti < 10] = 0
1 - (matrix_dti == 0).sum() / 188**2

# %% [markdown]
# <https://networkx.github.io/documentation/latest/reference/classes/graph.html>
#
# <https://networkx.github.io/documentation/latest/reference/generated/networkx.convert.to_networkx_graph.html#networkx.convert.to_networkx_graph>
#
# We can input a numpy matrix to the constructor
# (among many other input formats)

# %%
G = nx.Graph(matrix)
G.nodes()

# %%
# G.edges()

# %%
# One "row" the connections from node 0 to others.
G.adj[0]

# %%
G_dti = nx.Graph(matrix_dti)
G_dti.nodes()

# %%
nx.density(G)

# %%
nx.density(G_dti)

# %%
G.degree()

# %%
with open("dti_avg_region_names_full_file.txt") as f:
    read_data = f.read()

indices = read_data.split(sep="\n")[:-1]

df = pd.DataFrame(index=indices)
df["Degree"] = list(dict(G.degree()).values())
df.Degree.sort_values(ascending=False)

# %%
df_dti = pd.DataFrame(index=indices)
df_dti["Degree"] = list(dict(G_dti.degree()).values())

# %%
df["Degree"].hist()

# %%
df_dti["Degree"].hist()

# %%
pd.plotting.boxplot(df["Degree"])

# %%
# Do the same nodes in each network have similar degree?
plt.scatter(df["Degree"], df_dti["Degree"])

# %% [markdown]
# # Clustering
#
# <https://networkx.github.io/documentation/stable/reference/algorithms/clustering.html>
#
# <https://en.wikipedia.org/wiki/Clustering_coefficient>
#
# A clustering coefficient is a measure of:
# the degree to which nodes in a graph tend to cluster together...
#
# ![image2.png](./images/image2.png)

# %% [markdown]
# ## What is a triangle with a node?
#
# ![image3.png](./images/image3.png)
#
# Finds the number of triangles that include a node as one vertex:

# %%
df["Triangles"] = list(nx.triangles(G).values())
df.Triangles.sort_values(ascending=False)

# %% [markdown]
# ## Clustering cofficient defined in terms of triangles
#
# ### For unweigted graphs:
#
# $c_u = \frac{2 T(u)}{deg(u)(deg(u)-1)}$
#
# where:
# $T(u)$ is the number of triangles through node $u$, and
# $deg(u)$ is the degree of $u$.
#
# ### For weighted graphs
#
# For weighted graphs, the clustering is defined as:
# the geometric average of the subgraph edge weights
#
# $c_u = \frac{1}{deg(u)(deg(u)-1))} \sum_{uv} (\hat{w}_{uv} \hat{w}_{uw} \hat{w}_{vw})^{1/3}$
#
# The edge weights $\hat{w}_{uv}$ are normalized,
# by the maximum weight in the network:
# $\hat{w}_{uv} = w_{uv} / max(w)$.
# The value of $c_u$ is assigned to 0 if $deg(u) < 2$

# %%
df["Clustering"] = list(nx.clustering(G).values())
print(df["Clustering"].mean())
df.Clustering.sort_values(ascending=False)

# %%
df["Clustering_weighted"] = list(nx.clustering(G, weight="weight").values())
print(df["Clustering_weighted"].mean())
df.Clustering_weighted.sort_values(ascending=False)

# %%
nx.average_clustering(G)

# %%
nx.average_clustering(G, weight="weight")

# %%
plt.rcParams["figure.figsize"] = (15, 15)
pd.plotting.scatter_matrix(df)

# %% [markdown]
# # Shortest paths
#
# <https://en.wikipedia.org/wiki/Shortest_path_problem>
#
# <https://networkx.github.io/documentation/stable/reference/algorithms/shortest_paths.html>
#
# The shortest path problem:
# Find a path between two vertices (or nodes) in a graph,
# such that the sum of the weights of its constituent edges is minimized.
#
# ![image4.png](./images/image4.png)

# %% [markdown]
# There can be multiple shortest paths:
#
# ![image5.png](./images/image5.png)

# %%
# Shortest path from 0 to all:
nx.shortest_path(G, 0)

# %%
# shortest path from 1 to all:
nx.shortest_path(G, 1)

# %%
nx.shortest_path(G, 0, weight="weight")

# %%
nx.shortest_path(G, 0, 1)

# %%
nx.shortest_path(G, 0, 1, weight="weight")

# %%
list(nx.all_shortest_paths(G, 0, 1))

# %%
list(nx.all_shortest_paths(G, 0, 5))

# %%
list(nx.all_shortest_paths(G, 0, 1, weight="weight"))

# %%
list(nx.all_shortest_paths(G, 0, 5, weight="weight"))

# %%
nx.shortest_path_length(G, 0, 1)

# %%
nx.shortest_path_length(G, 0, 1, weight="weight")

# %% [markdown]
# # Centrality
#
# <https://en.wikipedia.org/wiki/Centrality>
#
# <https://networkx.github.io/documentation/stable/reference/algorithms/centrality.html>
#
# In graph theory and network analysis,
# indicators of centrality identify the most important vertices within a graph.
#
# Applications include:
#
# -   identifying the most influential person(s) in a social network,
# -   key infrastructure nodes in the Internet or urban networks, and
# -   super-spreaders of disease.
#
# ![image6.png](./images/image6.png)
#
# Examples of
#
# A\) Betweenness centrality, B) Closeness centrality,
#
# C\) Eigenvector centrality, D) Degree centrality,
#
# E\) Harmonic Centrality and F) Katz centrality of the same graph.

# %% [markdown]
# ## Degree centrality
#
# is just degree (we plotted that above)

# %% [markdown]
# ## Closeness Centrality
#
# <https://en.wikipedia.org/wiki/Closeness_centrality>
#
# In order to properly define closeness we need to define the term farness:
#
# -   Distance between two nodes is the shortest paths between them.
# -   The farness of a node is the sum of distances between that node and all other nodes.
#
# And the closeness of a node is the inverse of its farness.
# It is the normalized inverse of the sum of topological distances to that node in the graph.
#
# The most central node is the node that propagates information the fastest through the network.
# The description of closeness centrality makes it similar to the degree centrality.
#
# Is the highest degree centrality always the highest closeness centrality?
# No.
# Think of the example where one node connects two components,
# that node has a low degree centrality but a high closeness centrality (A?).
#
# ![image7.png](./images/image7.png)
#
# **In a connected graph,
# closeness centrality (or closeness) of a node is a measure of centrality in a network,
# calculated as the sum of the length of the shortest paths between the node and all other nodes in the graph.
# Thus the more central a node is,
# the closer it is to all other nodes.**
#
# In networkx, closeness centrality of a node $u$ is the reciprocal of the
# average shortest path distance to $u$ over all $n-1$ reachable nodes.
#
# $C(u) = \frac{n - 1}{\sum_{v=1}^{n-1} d(v, u)},$
#
# where $d(v, u)$ is the shortest-path distance between $v$ and $u$, and
# $n$ is the number of nodes that can reach $u$.
#
# Notice that higher values of closeness indicate higher centrality.

# %% [markdown]
# ![image8.png](./images/image8.png)

# %%
df["Closeness_centrality"] = list(nx.closeness_centrality(G).values())
df.Closeness_centrality.sort_values(ascending=False)

# %% [markdown]
# ## Betweenness Centrality
#
# <https://en.wikipedia.org/wiki/Betweenness_centrality>
#
# -   Betweenness centrality quantifies the number of times a node acts as a bridge,
#     along the shortest path between two other nodes.
# -   It is the number of shortest paths in the graph that pass through the node,
#     divided by the total number of shortest paths.
# -   Computes all the shortest paths between every pair of nodes,
#     and sees what is the percentage of that passes through node k.
#     That percentage gives us the centrality for node k.
# -   Nodes with high betweenness centrality control information flow in a network.
#
# ![image9.png](./images/image9.png) Hue (from red = 0 to blue =
# max) shows the node betweenness.
#
# ## In networkx:
#
# Betweenness centrality of a node $v$ is the sum of the fraction of all-pairs shortest paths that pass through $v$
#
# $c_B(v) =\sum_{s,t \in V} \frac{\sigma(s, t|v)}{\sigma(s, t)}$
#
# where $V$ is the set of nodes,
# $\sigma(s, t)$ is the number of shortest $(s,t)$-paths,
# and $\sigma(s, t|v)$ is the number of those paths passing through some node $v$ other than $s,t$.
#
# If $s=t$, then $\sigma(s, t) = 1$, and if $v \in {s, t}$, then
# $\sigma(s, t|v) = 0$

# %%
df["Betweenness_centrality"] = list(nx.betweenness_centrality(G).values())
df.Betweenness_centrality.sort_values(ascending=False)

# %% [markdown]
# # Eigenvector Centrality
#
# <https://en.wikipedia.org/wiki/Eigenvector_centrality>
#
# -   The eigenvector centrality extends the concept of a degree.
# -   The best to think of it is the average of the centralities of it's network neighbors.
# -   Eigenvector centrality (also called eigencentrality) is a measure of the influence of a node in a network.
#     Relative scores are assigned to all nodes in the network,
#     based on the concept that:
#     connections to high-scoring nodes contribute more to the score of the node in question,
#     than equal connections to low-scoring nodes.
# -   A high eigenvector score means that a node is connected to many nodes,
#     who themselves have high scores.
# -   Eigenvector centrality computes the centrality for a node,
#     based on the centrality of its neighbors.
#
# ![image10.png](./images/image10.png)

# %%
df["Eigenvector_Centrality"] = list(nx.eigenvector_centrality(G).values())
df.Eigenvector_Centrality.sort_values(ascending=False)

# %%
plt.rcParams["figure.figsize"] = (20, 20)
pd.plotting.scatter_matrix(df)

# %% [markdown]
# ## Review
#
# ![image11.png](./images/image11.png)

# %% [markdown]
# # Efficiency
#
# <https://en.wikipedia.org/wiki/Efficiency_(network_science)>
#
# Efficiency of a network is a measure of how efficiently it exchanges information.
# The concept of efficiency can be applied to both local and global scales in a network.
# On a global scale, efficiency quantifies the exchange of information,
# across the whole network where information is concurrently exchanged.
# The local efficiency quantifies a network's resistance to failure on a small scale.
# That is the local efficiency of a node i,
# characterizes how well information is exchanged by its neighbors,
# when it is removed.
#
# -   The **efficiency** of a pair of nodes in a graph is:
#     the multiplicative inverse of the shortest path distance between the nodes.
# -   The **average global efficiency** of a graph is:
#     the average efficiency of all pairs of nodes
# -   The **local efficiency** of a node in the graph is:
#     the average global efficiency of the subgraph induced by the neighbors of the node.
# -   The **average local efficiency** is:
#     the average of the local efficiencies of each node

# %%
nx.global_efficiency(G)

# %%
nx.local_efficiency(G)

# %%
nx.shortest_path_length(G, 0, 1)

# %% [markdown]
# Multiplicative inverse or reciprocal for a shortest path dist,
# $1/x$ or $x^{−1}$

# %%
nx.efficiency(G, 0, 1)

# %% [markdown]
# # Rich club coefficient
#
# <https://en.wikipedia.org/wiki/Rich-club_coefficient>
#
# <https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.richclub.rich_club_coefficient.html#networkx.algorithms.richclub.rich_club_coefficient>
#
# The rich-club coefficient is a metric on graphs and networks,
# designed to measure the extent to which well-connected nodes also connect to each other.
# Networks which have a relatively high rich-club coefficient,
# are said to demonstrate the rich-club effect,
# and will have many connections between nodes of high degree.
# This effect has been measured and noted on topics like:
# scientific collaboration networks and air transportation networks.
# It has been shown to be significantly lacking on protein interaction networks.
#
# The rich-club coefficient of a network is useful as a heuristic measurement of the robustness of a network.
# A high rich-club coefficient implies that the hubs are well connected,
# and global connectivity is resilient to any one hub being removed.
# It is also useful for verifying theories that generalize to other networks.
# For example,
# the consistent observation of high rich-club coefficients for scientific collaboration networks,
# adds evidence to the theory that within social groups,
# the elite tend to associate with one another.
#
# For each degree $k$,
# the rich-club coefficient is the ratio of the number of actual,
# to the number of potential edges,
# for nodes with degree greater than $k$:
#
# $\phi(k) = \frac{2 E_k}{N_k (N_k - 1)}$
#
# where $N_k$ is the number of nodes with degree larger than $k$, and
# $E_k$ is the number of edges among those nodes.
#
# ![image12.png](./images/image12.png)

# %%
# help(nx.rich_club_coefficient)
Rich_club = nx.rich_club_coefficient(G, normalized=False)
print("{Degree: coefficient,")
Rich_club

# %% [markdown]
# # Assortivitiy
#
# <https://en.wikipedia.org/wiki/Assortativity>
#
# <https://networkx.github.io/documentation/stable/reference/algorithms/assortativity.html>
#
# Birds of a feather, flock together...
#
# Assortativity, or assortative mixing is a preference for a network's nodes to attach to others that are similar in some way.
# Though the specific measure of similarity may vary,
# network theorists often examine assortativity in terms of a node's degree.
# Assortativity measures the similarity of connections in the graph with respect to the node degree.
#
# ![image13.png](./images/image13.png)
#
# Scale-free networks for different degrees of assortativity:
# (a) A = 0 (uncorrelated network),
# (b) A = 0.26,
# (c) A = 0.43, where A indicates r, the assortativity coefficient
#
# The assortativity coefficient is:
# the Pearson correlation coefficient of degree between pairs of linked nodes.
# Positive values of r indicate a correlation between nodes of similar degree,
# while negative values indicate relationships between nodes of different degree.
# In general, r lies between −1 and 1.
# When r = 1, the network is said to have perfect assortative mixing patterns,
# when r = 0 the network is non-assortative,
# while at r = −1 the network is completely disassortative.
#
# ## Applications
#
# The properties of assortativity are useful in the field of epidemiology,
# since they can help understand the spread of disease or cures.
# For instance, the removal of a portion of a network's vertices may correspond to curing,
# vaccinating, or quarantining individuals or cells.
# Since social networks demonstrate assortative mixing,
# diseases targeting high degree individuals,
# are likely to spread to other high degree nodes.
# Alternatively, within the cellular network—which,
# as a biological network is likely dissortative—vaccination strategies,
# that specifically target the high degree vertices,
# may quickly destroy the epidemic network.

# %%
nx.degree_assortativity_coefficient(G)

# %% [markdown]
# # Degeneracy
#
# <https://en.wikipedia.org/wiki/Degeneracy_(graph_theory)#k-Cores>
#
# <https://networkx.github.io/documentation/stable/reference/algorithms/core.html>
#
# In graph theory, a **k-degenerate** graph is an undirected graph,
# in which every subgraph has a vertex of degree at most k:
# that is, some vertex in the subgraph touches k or fewer of the subgraph's edges.
#
# The connected components that are left,
# after all vertices of degree less than k have been removed,
# are called the **k-cores** of the graph,
# and the **degeneracy** of a graph is:
# the largest value k such that it has a k-core.
# The degeneracy of a graph is the smallest value of k,
# for which it is k-degenerate.
# The degeneracy of a graph is a measure of how sparse it is,
# and is within a constant factor of other sparsity measures,
# such as the arboricity of a graph.
# Degeneracy is also known as the **k-core number**
#
# A **k-core** is a maximal subgraph,
# that contains nodes of degree k or more.
# A k-core of a graph G is a maximal subgraph of G,
# in which all vertices have degree at least k.
# It is one of the connected components of the subgraph of G,
# formed by repeatedly deleting all vertices of degree less than k.
# If a non-empty k-core exists, then, clearly,
# G has degeneracy at least k, and
# the degeneracy of G is the largest k for which G has a k-core.
#
# The **core number** of a node is the largest value k of a k-core containing that node.
#
# ![image14.png](./images/image14.png)
#
# A 2-degenerate graph: each vertex has at most two neighbors to its left,
# so the rightmost vertex of any subgraph has degree at most two.
# Its 2-core, the subgraph remaining,
# after repeatedly deleting vertices of degree less than two, is shaded.
#
# ![image15.png](./images/image15.png)

# %%
df["Core_num"] = list(nx.core_number(G).values())
df.Core_num.sort_values(ascending=False)

# %%
G_kcore = nx.k_core(G)
G_kcore.nodes()

# %% [markdown]
# # Eccentricity and diameter (max degrees of separation)
#
# Maximum degrees of separation in this graph at this threshold?
#
# <https://en.wikipedia.org/wiki/Distance_(graph_theory)>
#
# <https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.distance_measures.eccentricity.html#networkx.algorithms.distance_measures.eccentricity>
#
# The **eccentricity** $\epsilon (v)$ of a vertex $v$ is the greatest geodesic distance between $v$ and any other vertex.
# It can be thought of as how far a node is from the node most distant from it in the graph.
#
# The **diameter** is the maximum eccentricity.

# %%
df["eccentricity"] = list(nx.eccentricity(G).values())
df.eccentricity.sort_values(ascending=False)

# %%
nx.diameter(G)

# %% [markdown]
# # Emergence of connected components
#
# As we change the threshold for cutting connectivity,
# when does the network fragment into multiple connected components?
#
# What is the minimum percent connectivity for still being 1 component?
#
# ![image16.png](./images/image16.png)

# %%
# Progressively drop the weights at which binarization happens:
test = matrix.copy()
density = []
components = []
diameter = []
for i in np.array(range(0, 100, 1)) / 100.0:
    test[test < i] = 0
    H = nx.Graph(test)
    density.append(nx.density(H))
    components.append(nx.number_connected_components(H))

# %%
plt.rcParams["figure.figsize"] = (10, 10)
plt.plot(density, components, linewidth=3)

# %%
# x is linear increments of cut threshold
plt.plot(list(range(0, 100)), components, linewidth=3)

# %% [markdown]
# # Diameter as we vary threshold for binarizing
#
# Diameter is max eccentricity, how about mean?

# %%
test = matrix.copy()
diameter = []
for i in np.array(range(0, 39, 1)) / 100.0:
    test[test < i] = 0
    H = nx.Graph(test)
    diameter.append(nx.diameter(H))

# %%
plt.plot(list(range(0, 39)), diameter, linewidth=3)

# %%
plt.rcParams["figure.figsize"] = (20, 20)
pd.plotting.scatter_matrix(df)

# %% [markdown]
# # Minimum and maximum spanning trees (MST)
#
# <https://en.wikipedia.org/wiki/Minimum_spanning_tree>
#
# <https://networkx.github.io/documentation/stable/reference/algorithms/tree.html#module-networkx.algorithms.tree.mst>
#
# A minimum spanning tree (MST) or minimum weight spanning tree is:
# a subset of the edges of a connected, edge-weighted (un)directed graph,
# that connects all the vertices together,
# without any cycles and with the minimum possible total edge weight.
#
# That is, it is a spanning tree whose sum of edge weights is as small as possible.
#
# ![image17.png](./images/image17.png)
#
# A planar graph and its minimum spanning tree.
# Each edge is labeled with its weight,
# which here is roughly proportional to its length.

# %%
# Node size related to degree, and edge color related to weight

plt.rcParams["figure.figsize"] = (20, 20)
T = nx.maximum_spanning_tree(G)
edges, weights = zip(*nx.get_edge_attributes(T, "weight").items())
d = nx.degree(T)
pos = nx.spring_layout(T)
nx.draw(
    T,
    pos,
    nodelist=dict(nx.degree(T)).keys(),
    node_size=[v * 100 for v in dict(nx.degree(T)).values()],
    node_color="y",
    edgelist=edges,
    edge_color=weights,
    edge_cmap=plt.cm.Blues,
    with_labels=True,
    width=10.0,
)
plt.show()

# %%
plt.rcParams["figure.figsize"] = (20, 20)
T = nx.minimum_spanning_tree(G)
edges, weights = zip(*nx.get_edge_attributes(T, "weight").items())
d = nx.degree(T)
pos = nx.spring_layout(T)
nx.draw(
    T,
    pos,
    nodelist=dict(nx.degree(T)).keys(),
    node_size=[v * 100 for v in dict(nx.degree(T)).values()],
    node_color="y",
    edgelist=edges,
    edge_color=weights,
    edge_cmap=plt.cm.Blues,
    with_labels=True,
    width=10.0,
)
plt.show()

# %% [markdown]
# # Making meaning of connectivity and graph features
#
# ![image18.png](./images/image18.png)

# %% [markdown]
# ![image19.png](./images/image19.png)

# %% [markdown]
# Inputs to the brain:
#
# ![image20.png](./images/image20.png)

# %% [markdown]
# ![image21.png](./images/image21.png)

# %% [markdown]
# ![image22.png](./images/image22.png)

# %% [markdown]
# ![image23.png](./images/image23.png)

# %% [markdown]
# Distance from inputs
#
# ![image24.png](./images/image24.png)

# %% [markdown]
# Matrix processing
#
# ![image25.png](./images/image25.png)

# %% [markdown]
# Mean distance from inputs
#
# ![image26.png](./images/image26.png)

# %% [markdown]
# MST with mean distance from inputs in node color
#
# ![image27.png](./images/image27.png)

# %% [markdown]
# Function of deep versus shallow regions
#
# ![image28.png](./images/image28.png)

# %% [markdown]
# ![image29.png](./images/image29.png)
